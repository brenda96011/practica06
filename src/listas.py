from src.nodos import Nodo


# Definicion de la clase 'ListaLigada':
class ListaLigada:
    def __init__(self):
        self.cabeza = None

    # Agrega un elemento al final de la lista
    def agregar_elemento(self, valor):
        nodito = Nodo(valor)
        if self.cabeza == None:
            self.cabeza = nodito
        else:
            actual = self.cabeza
            while actual.siguiente != None:
                actual = actual.siguiente
            actual.siguiente = nodito
        return f"Se ha añadido el elemento {valor} a la lista"

    def buscar_elemento(self, valor_a_buscar):
        if self.cabeza != None:
            actual = self.cabeza
            while actual != None:
                if actual.valor == valor_a_buscar:
                    return actual
                actual = actual.siguiente
        return None

    def elimina_cabeza(self):
        if self.cabeza != None:
            self.cabeza = self.cabeza.siguiente
        return "La cabeza de la lista ha sido eliminada"

    def elimina_rabo(self):
        if self.cabeza != None:
            if self.cabeza.siguiente == None:
                self.cabeza = None
            else:
                ultimo = self.cabeza
                while ultimo.siguiente.siguiente != None:
                    ultimo = ultimo.siguiente
                ultimo.siguiente = None

        return "El rabo de la lista ha sido eliminado"

    def tamagno(self):
        actual = self.cabeza
        contador = 0
        while actual != None:
            actual = actual.siguiente
            contador += 1
        return contador

    def copia(self):
        nueva_lista = ListaLigada()
        actual = self.cabeza
        while actual:
            nueva_lista.agregar_elemento(actual.valor)
            actual = actual.siguiente
        return nueva_lista

    def __str__(self):
        valores = []
        actual = self.cabeza
        while actual:
            valores.append(str(actual.valor))
            actual = actual.siguiente
        return " -> ".join(valores)
